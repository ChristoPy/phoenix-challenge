# Phoenix / Elixir Challenge

Este é um breve teste para a vaga de developer Elixir/Phoenix.

**Objetivo**: Construir um web forum com _threads_ e _comments_ seguindo [este Mockup](https://www.dropbox.com/s/04fglhzts5xn2ma/forum.pdf?dl=0)

Algumas observações:

- Não é necessário se preocupar com deployment. Rodando local, sem erros, é suficiente.

- Inclua, no README, informações de como rodar o projeto.

- Lembre-se de incluir dados de "seed", a serem inseridos durante o setup do projeto e exibidos quando o app é visitado pela primeira vez.

- A escrita de testes é altamente recomendada. Foque apenas em testes de controller.

- Adicione validações necessárias.

- Não é necessário adicionar autenticação.

- Durante o trabalho, crie commits agrupando funcionalidades relacionadas e utilize mensagens de commit descritivas.

- Ao concluir, faça um `push` para uma topic branch e crie um PR/MR.

- Divirta-se! Caso haja dúvidas, fique a vontade para entrar em contato conosco :)
